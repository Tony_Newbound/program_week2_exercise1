﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_9_b
{
    class Program
    {
        static void Main(string[] args)
        {
            var length = 0;
            var breadth = 0;

            Console.WriteLine("Please Enter the length");
            length = int.Parse(Console.ReadLine());

            Console.WriteLine("Please Enter the breadth");
            breadth = int.Parse(Console.ReadLine());

            Console.WriteLine($"The Length*2 x Breadth*2 is {(length*2) * (breadth*2)}");
        }
    }
}
