﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week_2_exercise_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number less than 20");
            var i = int.Parse(Console.ReadLine());
            var counter = 20;


            do
            {
                var j = i + 1;
                Console.WriteLine($"This line is {j}");
                i++;
            }

            while (i < counter);

        }
    }
}
