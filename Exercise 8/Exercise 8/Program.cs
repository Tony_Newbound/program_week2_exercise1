﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_8
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 0;
            var b = 0;
            var c = 0;
            var d = 0;
            var e = 0;

            Console.WriteLine("Please enter a number");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("And another number if you please");
            b = int.Parse(Console.ReadLine());

            Console.WriteLine("And for the 3rd time");
            c = int.Parse(Console.ReadLine());

            Console.WriteLine("And again");
            d = int.Parse(Console.ReadLine());

            Console.WriteLine("Last time");
            e = int.Parse(Console.ReadLine());

            Console.WriteLine($"When you use the equation you get  {(a + b) * c / (d - e)}");
        }
    }
}
