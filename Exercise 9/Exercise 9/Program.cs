﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_9
{
    class Program
    {
        static void Main(string[] args)
        {
            var length = 0;
            var breadth = 0;

            Console.WriteLine("Please Enter the length");
            length = int.Parse(Console.ReadLine());

            Console.WriteLine("Please Enter the breadth");
            breadth = int.Parse(Console.ReadLine());

            Console.WriteLine($"The Length x Breadth is {length * breadth}");

        }
    }
}
