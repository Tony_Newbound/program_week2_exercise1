﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week_2_exercise_6
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 100;
            var i = 1;

            while (i <= counter)

            {
                
                Console.WriteLine($"The number of this line is {i}");
                i += 1;
            }
        }
    }
}
