﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var yourName = "";

            Console.WriteLine("Go will ya tell us ya name");
            yourName = Console.ReadLine();


            Console.WriteLine($"Hi There {yourName} would you like a cookie?");
        }
    }
}
