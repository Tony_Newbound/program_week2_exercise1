﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_9_c
{
    class Program
    {
        static void Main(string[] args)
        {
            var r = 0;
            const double a = 3.14;

            Console.WriteLine("Please enter the radius of the circle");
            r = int.Parse(Console.ReadLine());

            Console.WriteLine($"The area of the circle is {a * (r * r)}");
        }
    }
}
