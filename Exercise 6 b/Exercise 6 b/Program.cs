﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_6_b
{
    class Program
    {
        static void Main(string[] args)
        {
            var kms = 0;
            var miles = 0.621371;

            Console.WriteLine("Please enter the amount of kms you have traveled?");
            kms = int.Parse(Console.ReadLine());

            Console.WriteLine($"You have traveled {kms * miles} miles");
        }
    }
}
