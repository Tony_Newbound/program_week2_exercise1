﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_6
{
    class Program
    {
        static void Main(string[] args)
        {
            var miles = 0;
            const double kms = 1.609344;
            Console.WriteLine("Please enter the distance in miles you have traveled.");
            miles = int.Parse(Console.ReadLine());

            Console.WriteLine($"The amount of kms u traveled is {miles * kms}");
        }
    }
}
